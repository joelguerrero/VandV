## Reproduction of benchmark test case A formulated in AIJ guidelines for practical applications of CFD to pedestrian wind environment around buildings

- Creator: Masashi IMANO <masashi.imano@gmail.com>
- Supported OpenFOAM version: v1906, v1912

### Prerequisites

Gnuplot utility are required in order to run the case.

###  What is this?

This case solves benchmark test case A defined in AIJ guidelines for practical applications of CFD to pedestrian wind environment around a high-rise building [E1] following the benchmark procedure.

### Running the case
In order to run the case:

1. Type ./Allrun. This script will automatically make mesh, perform CFD calculation.
1. After the run, you will find profileU.eps and profilek.eps which compare the OpenFOAM results with those of a wind tunnel test [E2].
1. Type ./Allclean to clean up the case directory and reset to the initial state.

### References
- [E1] Architectural Institute of Japan, Guidebook for CFD Predictions of Urban Wind Environment, 2020. URL: http://www.aij.or.jp/jpn/publish/cfdguide/index_e.htm
- [E2] Mochida, A., Tominaga, Y., Murakami, S., Yoshie, R., Ishihara, T., Ooka, R., 2002. Comparison of various k-epsilon model and DSM applied to flow around a high-rise building - report on AIJ cooperative project for CFD prediction of wind environment -, Wind & Structures 5, No.2-4, 227-244.  URL: http://www.aij.or.jp/jpn/publish/cfdguide/R02_6.pdf

## Disclaimer:
OPENFOAM(R) is a registered trade mark of ESI Group,
the producer of the OpenFOAM software and owner of the OPENFOAM(R) trade marks.
This offering is not approved or endorsed by ESI Group.
