# state file generated using paraview version 5.6.2

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.6.2
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1940, 1276]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.OrientationAxesLabelColor = [0.0, 0.0, 0.0]
renderView1.OrientationAxesOutlineColor = [0.0, 0.0, 0.0]
renderView1.CenterOfRotation = [0.4000000059604645, 0.0, 0.44999998807907104]
renderView1.StereoType = 0
renderView1.CameraPosition = [-3.851127251115476, 0.0, 0.44999998807907104]
renderView1.CameraFocalPoint = [0.4000000059604645, 0.0, 0.44999998807907104]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 0.4661700432310288
renderView1.CameraParallelProjection = 1
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.XTitleFontFile = ''
renderView1.AxesGrid.YTitleFontFile = ''
renderView1.AxesGrid.ZTitleFontFile = ''
renderView1.AxesGrid.XLabelFontFile = ''
renderView1.AxesGrid.YLabelFontFile = ''
renderView1.AxesGrid.ZLabelFontFile = ''

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(FileName='pv.foam')

# create a new 'STL Reader'
buildingstl = STLReader(FileNames=['constant/triSurface/building.stl'])

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pvfoam
pvfoamDisplay = Show(pvfoam, renderView1)

# trace defaults for the display properties.
pvfoamDisplay.Representation = 'Surface With Edges'
pvfoamDisplay.ColorArrayName = ['POINTS', '']
pvfoamDisplay.SpecularColor = [0.0, 0.0, 0.0]
pvfoamDisplay.Ambient = 1.0
pvfoamDisplay.EdgeColor = [0.0, 0.0, 0.0]
pvfoamDisplay.BackfaceRepresentation = 'Cull Frontface'
pvfoamDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pvfoamDisplay.SelectOrientationVectors = 'None'
pvfoamDisplay.ScaleFactor = 0.16800000071525575
pvfoamDisplay.SelectScaleArray = 'None'
pvfoamDisplay.GlyphType = 'Arrow'
pvfoamDisplay.GlyphTableIndexArray = 'None'
pvfoamDisplay.GaussianRadius = 0.008400000035762786
pvfoamDisplay.SetScaleArray = [None, '']
pvfoamDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.OpacityArray = [None, '']
pvfoamDisplay.OpacityTransferFunction = 'PiecewiseFunction'
pvfoamDisplay.DataAxesGrid = 'GridAxesRepresentation'
pvfoamDisplay.SelectionCellLabelFontFile = ''
pvfoamDisplay.SelectionPointLabelFontFile = ''
pvfoamDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
pvfoamDisplay.DataAxesGrid.XTitleFontFile = ''
pvfoamDisplay.DataAxesGrid.YTitleFontFile = ''
pvfoamDisplay.DataAxesGrid.ZTitleFontFile = ''
pvfoamDisplay.DataAxesGrid.XLabelFontFile = ''
pvfoamDisplay.DataAxesGrid.YLabelFontFile = ''
pvfoamDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
pvfoamDisplay.PolarAxes.PolarAxisTitleFontFile = ''
pvfoamDisplay.PolarAxes.PolarAxisLabelFontFile = ''
pvfoamDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
pvfoamDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# show data from buildingstl
buildingstlDisplay = Show(buildingstl, renderView1)

# trace defaults for the display properties.
buildingstlDisplay.Representation = 'Feature Edges'
buildingstlDisplay.AmbientColor = [0.0, 0.0, 0.0]
buildingstlDisplay.ColorArrayName = ['POINTS', '']
buildingstlDisplay.LineWidth = 4.0
buildingstlDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
buildingstlDisplay.SelectOrientationVectors = 'None'
buildingstlDisplay.ScaleFactor = 0.015999999642372132
buildingstlDisplay.SelectScaleArray = 'STLSolidLabeling'
buildingstlDisplay.GlyphType = 'Arrow'
buildingstlDisplay.GlyphTableIndexArray = 'STLSolidLabeling'
buildingstlDisplay.GaussianRadius = 0.0007999999821186066
buildingstlDisplay.SetScaleArray = [None, '']
buildingstlDisplay.ScaleTransferFunction = 'PiecewiseFunction'
buildingstlDisplay.OpacityArray = [None, '']
buildingstlDisplay.OpacityTransferFunction = 'PiecewiseFunction'
buildingstlDisplay.DataAxesGrid = 'GridAxesRepresentation'
buildingstlDisplay.SelectionCellLabelFontFile = ''
buildingstlDisplay.SelectionPointLabelFontFile = ''
buildingstlDisplay.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
buildingstlDisplay.DataAxesGrid.XTitleFontFile = ''
buildingstlDisplay.DataAxesGrid.YTitleFontFile = ''
buildingstlDisplay.DataAxesGrid.ZTitleFontFile = ''
buildingstlDisplay.DataAxesGrid.XLabelFontFile = ''
buildingstlDisplay.DataAxesGrid.YLabelFontFile = ''
buildingstlDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
buildingstlDisplay.PolarAxes.PolarAxisTitleFontFile = ''
buildingstlDisplay.PolarAxes.PolarAxisLabelFontFile = ''
buildingstlDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
buildingstlDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------
renderView1.ViewSize = [1920, 1080]
renderView1.CameraParallelScale = 0.56
##
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraPosition = [-3.85, 0.0, 0.5]
renderView1.CameraFocalPoint = [0.4, 0.0, 0.5]
file="meshX.png"
SaveScreenshot(file, renderView1, ImageResolution=renderView1.ViewSize)
print(file)
##
renderView1.CameraPosition = [0.4, -4.25, 0.5]
renderView1.CameraFocalPoint = [0.4, 0.0, 0.5]
file="meshY.png"
SaveScreenshot(file, renderView1, ImageResolution=renderView1.ViewSize)
print(file)
renderView1.CameraViewUp = [0.0, 1.0, 0.0]
renderView1.CameraPosition = [0.4, 0.0, 4.7]
renderView1.CameraFocalPoint = [0.4, 0.0, 0.5]
file="meshZ.png"
SaveScreenshot(file, renderView1, ImageResolution=renderView1.ViewSize)
print(file)
